# Map My Run Data

I was trying to figure out how far I'd run last year, and in the process, I discovered that Map My Run (and presumably its sibling apps) allow you to [download your workout data](https://www.mapmyfitness.com/auth/login?next=/workout/export/csv). So I wrote a simple python script to answer my question. I may add more later.

## Setup

This uses [pipenv](https://pipenv.readthedocs.io/en/latest/) to manage dependencies.

From the root directory, run `pipenv install`.

Create a data directory `mkdir data`.

Place the downloaded csv file in the newly created data directory, and rename it to `export.csv`.

Run `pipenv run python src/main.py`