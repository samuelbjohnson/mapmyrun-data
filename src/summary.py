import csv
from datetime import datetime
from functools import reduce
import re


date_regex = re.compile(r"(?P<month>\w*)\.?\s(?P<day>\d\d?),\s(?P<year>\d\d\d\d)")
months = ["", "Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"]


def parse_ua_date(date_string):
    m = date_regex.search(date_string)
    month = months.index(m.group("month"))
    day = int(m.group("day"))
    year = int(m.group("year"))
    return datetime(year, month, day)


def main():
    with open('data/export.csv') as csv_file:
        reader = csv.DictReader(csv_file)

        runs = [workout for workout in reader if parse_ua_date(workout['Workout Date']).year == 2019 and
                'Run' in workout['Activity Type']]
        distances = [float(run['Distance']) for run in runs]
        mileage = reduce((lambda a, b: a + b), distances)
        print("Found %d runs totalling %s miles" % (len(runs), mileage))


if __name__ == "__main__":
    main()